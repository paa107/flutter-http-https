/*
 * 1. Перед подключением http библиотеки нужно
 * прописать последнюю версию http в зависимости
 * файл: pubspec.yaml
 * 
 * http: ^0.12.2
 */

import 'package:flutter/material.dart';
// подключаем, "as" позволит использовать методы в объекте "http"
import 'package:http/http.dart' as http;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Работаем с сетевые запросы',
      home: TestHttp(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class TestHttp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => TestHttpState();
}

class TestHttpState extends State<TestHttp> {
  /*
   * Вариант 2. вывод в асинхронный метод
   */
  httpGet() async {
    try {
      var response = await http.get('https://json.flutter.su/echo');

      print("Respose status: ${response.statusCode}");
      print("Respose body: ${response.body}");
    } catch (error) {
      print("Error: $error");
    }
  }

  /*
   * 3. метод для Post запроса
   */
  httpPost() async {
    try {
      var response = await http.post('https://json.flutter.su/echo', body: {
        'name': 'my Name',
        'num': '15',
      }, headers: {
        'Accept': 'application/json'
      });

      print("Respose status: ${response.statusCode}");
      print("Respose body: ${response.body}");
    } catch (error) {
      print("Error: $error");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Сетевые запросы Http и Https'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FlatButton(
              color: Colors.blue,
              textColor: Colors.white,
              disabledColor: Colors.grey,
              disabledTextColor: Colors.black,
              padding: EdgeInsets.all(8.0),
              splashColor: Colors.blueAccent,
              /*
             * Варинат 1. использование метода "then" для
             * обработки http запросов
             * 
             * Ответ сервера выводится в консоль.
             * Если адрес есть, ответ будет: 200
             * Если адрес не существует, ответ: 404
             */
              // onPressed: () {
              //   http.get('https://json.flutter.su/echo').then((response) {
              //     print("Respose status: ${response.statusCode}");
              //     print("Respose body: ${response.body}");
              //   }).catchError((error) {
              //     print("Error: $error");
              //   });
              // },
              onPressed: httpGet,
              child: Text('http Get'),
            ),
            FlatButton(
              color: Colors.blue,
              textColor: Colors.white,
              disabledColor: Colors.grey,
              disabledTextColor: Colors.black,
              padding: EdgeInsets.all(8.0),
              splashColor: Colors.blueAccent,
              // 3.1. Кнопка отправки Post запроса
              onPressed: httpPost,
              child: Text('http Post'),
            ),
          ],
        ),
      ),
    );
  }
}
